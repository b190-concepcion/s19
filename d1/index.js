// console.log("Hello Batch 190");

//If statement
/*
	-executes a statement if a specified condition is true
	- can stand alom without the else statement
	
	Syntax:
		if(condition) {
			statement/code block
		}
		*/

		let numA = -1;

		if(numA < 0){
			console.log("Hello");
		}
		console.log(numA < 0);



		if(numA > 0){
			console.log("This statement will not be printed");
		}



		let city = "New York";

		if(city === "New York") {
			console.log("Welcome to New York City");
		}

//else if clause
/*
	-executes a statement if a previous conditions are false and if the specified condition is true
	-"else if" clause is optional and can be added to capture additional conditions to change the flow of a program
	*/
	let numB = 1;

	if(numA > 0) {
		console.log("Hello");
	}else if (numB > 0){
		console.log("World");
	}



	if(numA < 0) {
		console.log("Hello");
	}else if (numB > 0){
		console.log("World");
	}

	city = "Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York City!");
	}else if (city === "Tokyo"){
		console.log("Welcome to Tokyo");
	}

//else statement
/*
	-executes a statement if all other conditions are false
	-the "else" statement is optional and can be added to capture any other result to change the flow of our program

	*/

/*if (numA > 0) {
	console.log("Hello");
}else if (numB === 0){
	console.log("World");
}else{
	console.log("Again");
}

//Another Example

let age = prompt("Enter Your age: ");
if(age<=18){
	console.log("Now allowed to drink!");
}else {
	console.log("Matanda ka na, shot na!");
}*/

//Mini Activity



/*function getHeight(){

	let height = prompt("Enter Your height: ");
	console.log("Your Height is " +height);
	if(height<150){
		console.log("Did not passed the minimum height requirements");
	}else{
		console.log("Passed the minimum height requirements");
	}
}
getHeight();*/

//solution #2

function heightReq(h){

	if(h< 150){
		console.log("Did not passed the minimum height requirements");
	}else{
		console.log("Passed the minimum height requirements");
	}
}
heightReq(160);
heightReq(140);
heightReq(130);

let message = "no message. ";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed <30){
		return 'Not a Typhoon yet.';
	}else if (windSpeed <=61){
		return 'Tropical Depression detected';
	}else if(windSpeed >=62 && windSpeed <=88){
		return 'Tropical storm detected';
	}else if(windSpeed >=89 && windSpeed <=117){
		return 'Severe tropical storm detected';
	}else
	return 'Typhoon Detected';
}
message = determineTyphoonIntensity(69);
console.log(message);


if(message == 'Tropical storm detected'){
	console.warn(message);
}

//Truthy and Falsy 
/*
	- In Javascript, a truthy value is a value that is considered true when encountered in a Boolean context
	-Values are considered true unless defined otherwise
	Falsy values/exceptions for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
	*/
//Truthy Examples
if(true){
	console.log("Truthy!");
}

if(1){
	console.log("Truthy!");
}

if([]){
	console.log("Truthy!");
}

if(false){
	console.log("Truthy!");
}else{
	console.log("Falsy!");
}


//Conditional (Ternary Operator)
/*
	The ternary operator takes in three operands
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	Syntax:
		(condition) ? ifTrue : ifFalse
		*/


//Single statement execution

/*let ternaryResult =  (1 < 18) ? "statement is true" : "statement is false"

console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge() {
	name = 'john';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'jane';
	return 'Your are under the age limit';
}

let age = parseInt(prompt("What is your age? "));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in function: " + legalAge + ', ' + name);
*/


//Switch Statement
/*
	Syntax:
		switch (expression) {
			case value :
				statement
				break;
			default:
				statement;
				break;
		}
		*/

/*let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

	switch (day) {
		case 'monday':
		console.log("The color of the day is red");
		break;

		case 'tuesday':
		console.log("The color of the day is orange");
		break;

		case 'wednesday':
		console.log("The color of the day is yellow");
		break;

		case 'thursday':
		console.log("The color of the day is green");
		break;

		case 'friday':
		console.log("The color of the day is blue");
		break;

		case 'saturday':
		console.log("The color of the day is indigo");
		break;

		case 'sunday':
		console.log("The color of the day is violet");
		break;

		default:
		console.log("Please input a valid day!");
		break
	}*/



//Try-Catch-Finally Statement
function showIntensityAlert(windSpeed) {
	try{
		//attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	}

	catch(error){
		//catch errors within "try" statement
		console.log(error);
		console.warn(error.message);
	}

	finally {

		//continue execution of code regardless of success or failure of code execution in the "try" block to handle/resolve errors
		alert("Intensity updates will show new alert");
	}
}
showIntensityAlert(56);

